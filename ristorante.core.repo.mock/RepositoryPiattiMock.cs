using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ristorante.core.Entities;
using Ristorante.core.InterfacesRepo;

namespace ristorante.vore.repo.mock
{
    public class RepositoryPiattiMock : IRepositoryPiatti
    {
        private static List<Piatto> piatti = new List<Piatto>() {

            new Piatto{name="Carbonara", description="Rigatoni alla carbonara", typeOfPlate: "Primo", price:"12�"},
            new Piatto{name="Fiorentina", description="Bistecca alla fiorentina",typeOfPlate: "Secondo",price:"60�" },
        };

        public Piatto Add(Piatto item)
        {
            piatti.Add(item);
            return item;
        }

        public bool Delete(Piatto item)
        {
            return piatti.Remove(item);
        }

        public List<Piatto> GetAll()
        {
            return piatti;
        }

     
        public Piatto Update(Corso item)
        {
            var oldPiatto = GetByCode(item.PiattoCodice);
            piatti.Remove(oldPiatto);
            piatti.Add(item);
            return item;
        }
    }
}
