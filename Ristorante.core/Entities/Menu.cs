﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ristorante.core.Entities
{
    public class Menu
    {
        public string nameOfMenu { get; set; }

        public ICollection <Piatto> Piatti { get; set; }
    }
}
