﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ristorante.core.Entities
{
    public abstract class Piatto
    {
        public string name { get; set; }
        public string description { get; set; }
        public string typeOfPlate { get; set; } 
        public string price { get; set; }


    }
}
