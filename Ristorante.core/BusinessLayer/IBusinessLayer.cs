﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ristorante.core.BusinessLayer
{
    public interface IBusinessLayer
    {
        List<Piatto> GetAllPiatti();
        Esito InserisciNuovoPiatto(Piatto piatto);
        Esito ModificaPiatto(Piatto piattoModificato);
        Esito EliminaPiatto(string codice);

    }
}
