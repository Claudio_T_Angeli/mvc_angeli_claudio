﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ristorante.core.BusinessLayer
{
    public class MainBusinessLayer:IBusinessLayer
    {
        private readonly IRepositoryPiatti piattiRepo;
        private readonly IRepositoryMenu menuRepo;

        public MainBusinessLayer(IRepositoryPiatti piatti, IRepositoryMenu menu)
        {
            piattiRepo = piatti;
            menuRepo = menu;
        }


        public List<Piatti> GetAllPiatti()
        {
           return piattiRepo.GetAll();
        }

        public Esito InserisciNuovoPiatto(Piatto piatto)
        {
            if (piatto == null)
            {
                return new Esito{ IsOk = false, Messaggio="Piatto non esiste" };
            }
            
            Piatto piattoEsistente=piattiRepo.GetByCode(piatto.PiattoCodice);
            if(piattoEsistente != null)
            {
                return new Esito { IsOk = false, Messaggio = "Impossibile aggiungere il piatto, è lo stesso codice" };
            }
            piattiRepo.Add(piatto);
            return new Esito { IsOk = true, Messaggio = "Piatto aggiunto correttamente" };

        }

        public Esito ModificaPiatto(Piatto PiattoModificato)
        {
            var piattoEsistente=piattiRepo.GetByCode(piattoModificato.PiattoCodice);
            if (piattoEsistente == null)
            {
                return new Esito { IsOk = false, Messaggio = "Codice piatto errato e/o inesistente" };
            }
            piattiRepo.Update(piattoModificato);
            return new Esito { IsOk = true, Messaggio = "Il piatto è stato modificato" };
        }

        public Esito EliminaPiatto(string codice)

        {
            var piattoEsistente = piattiRepo.GetByCode(codice);
            if (piattoEsistente == null)
            {
                return new Esito { IsOk = false, Messaggio = "Codice piatto errato e/o inesistente" };
            }
            piattiRepo.Delete(piattoEsistente);
            return new Esito { IsOk = true, Messaggio = "Il piatto è stato eliminato" };
        }
    }
}
