﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ristorante.core.InterfaceRepo
{
    public interface IRepositoryPiatti
    {
         public Piatto GetByCode(string code);
    }
}
