﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Ristorante.Models
{
    public class PiattoViewModel
    {
        [DisplayName("Nome del piatto")]
        [Required]
        [MaxLength(10)]
        public string PiattoCodice { get; set; }
        [Required(ErrorMessage ="Campo obbligatorio")]
        public string Name { get; set; }        
        public string? Description { get; set; }        
    }
}
