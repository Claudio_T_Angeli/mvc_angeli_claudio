﻿using Microsoft.AspNetCore.Mvc;
using Ristorante.Models;
using System.Diagnostics;

namespace Ristorante.Controllers
{
    public class PiattiController : Controller
    {
        private readonly IBusinessLayer BL;

        public PiattiController(IBusinessLayer bl)
        {
            BL = bl;
        }

        public IActionResult Index()
        {
           
            List <Piatto> piatti = BL.GetAllPiatti();

            List <PiattoViewModel> PiattiViewModel = new List<PiattoViewModel>();

            foreach (var item in piatti)
            {
                piattiViewModel.Add(item.ToPiattoViewModel());
            }
            return View(piattiViewModel);
        }


    }
}